<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout'])->middleware('jwt.verify');
    Route::post('/refresh', [AuthController::class, 'refresh'])->middleware('jwt.verify');
    Route::get('/user-profile', [AuthController::class, 'userProfile'])->middleware('jwt.verify');
});

Route::apiResource('news', 'NewsController');
Route::apiResource('category', 'CategoryController');
