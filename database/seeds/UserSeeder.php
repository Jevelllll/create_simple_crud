<?php

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newsMaker = Role::where('slug','news-maker')->first();
        $superAdmin = Role::where('slug', 'super-admin')->first();

        $addNews = Permission::where('slug','add-news')->first();
        $editNews = Permission::where('slug','edit-news')->first();
        $deleteNews = Permission::where('slug','delete-news')->first();

        $user1 = new User();
        $user1->name = 'Vasily Pupkin';
        $user1->email = 'vasily@pupkin.com';
        $user1->password = bcrypt('secret');
        $user1->save();
        $user1->roles()->attach($newsMaker);
        $user1->permissions()->attach($addNews);
        $user1->permissions()->attach($editNews);
        $user1->permissions()->attach($deleteNews);

        $user2 = new User();
        $user2->name = 'Super Admin';
        $user2->email = 'super@admin.com';
        $user2->password = bcrypt('secret');
        $user2->save();
        $user2->roles()->attach($superAdmin);
    }
}
