<?php

use App\Models\Category;
use App\Models\News;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::whereEmail('vasily@pupkin.com')->first();

        DB::transaction(function() use ($user) {

            $categoryWorld = new Category();
            $categoryWorld->name = 'Мир';
            $categoryWorld->save();

            foreach ($this->setDataWorld() as $world) {
                $news = new News($world);
                $news->save();
                $categoryWorld->news()->attach($news);
                $user->news()->attach($news);
            }

            $categorySport = new Category();
            $categorySport->name = 'Спорт';
            $categorySport->save();

            foreach ($this->setDataSport() as $sport) {
                $news = new News($sport);
                $news->save();
                $categorySport->news()->attach($news);
                $user->news()->attach($news);
            }

            $categoryHealth = new Category();
            $categoryHealth->name = 'Здоровье';
            $categoryHealth->save();

            foreach ($this->setDataHealth() as $health) {
                $news = new News($health);
                $news->save();
                $categoryHealth->news()->attach($news);
                $user->news()->attach($news);
            }

            $categoryEconomy = new Category();
            $categoryEconomy->name = 'Экономика';
            $categoryEconomy->save();

            foreach ($this->setDataEconomy() as $economy) {
                $news = new News($economy);
                $news->save();
                $categoryEconomy->news()->attach($news);
                $user->news()->attach($news);
            }

            $categoryAuto = new Category();
            $categoryAuto->name = 'Авто';
            $categoryAuto->save();

            foreach ($this->setDataAuto() as $auto) {
                $news = new News($auto);
                $news->save();
                $categoryAuto->news()->attach($news);
                $user->news()->attach($news);
            }

        }, 2);

    }

    private function setDataWorld(): array
    {
        return [
            [
                'name' => 'Бананы оказались под угрозой исчезновения',
                'desc' => 'В мире могут исчезнуть бананы из-за высокоинфекционного почвенного грибка.'
            ],
            [
                'name' => 'В Африке впервые в истории сфотографировали редкую антилопу',
                'desc' => 'Доступные и простые в использовании автоматические камеры появились относительно недавно, однако уже стали незаменимым инструментов зоологов.'
            ],
            [
                'name' => 'Греция обещает открыться для туристов с 14 мая',
                'desc' => 'Греция откроется для туристов с 14 мая с соблюдением комплекса противокоронавирусных мероприятий. Об этом заявил министр туризма страны Харрис Теохарис в парламенте, передает Укринформ со ссылкой на E…'
            ],
            [
                'name' => 'Ученые подтвердили, что собаки могут ревновать хозяина',
                'desc' => 'Давно известно, что собаки могут раздражаться, если владелец будет на их глазах уделять внимание другому псу. Но тот факт, что животные могут воображать «измену» и переживать по этому поводу, был откр…'
            ]
        ];
    }

    private function setDataHealth(): array
    {
        return [
            [
                'name' => 'Чем опасны пробуждения среди ночи',
                'desc' => 'Отсутствие полноценного ночного отдыха может не только вызвать проблемы со здоровьем, но и быть признаком уже существующих. Если вы просыпаетесь ночью с определенным набором симптомов, это может быть…'
            ],
            [
                'name' => 'За сутки в Украине выявили более 17 тысяч новых случаев коронавируса',
                'desc' => 'По состоянию на утро, 16 апреля, в Украине всего коронавирус подтвердили у 1 921 244 человека, только за прошедшие сутки — 17 479 новых случаев заражения. Об этом сообщает министр здравоохранения Укра…'
            ]
        ];
    }

    private function setDataSport(): array
    {
        return [
            [
                'name' => 'Мотогонщик из Днепра стал призером Кубка Украины',
                'desc' => 'Известный гонщик из Днепра Семен Неруш впервые попробовал свои силы в самом мощном классе гонок и сразу взобрался на пьедестал Кубка Украины. Этот гонщик-вундеркинд в седло мотоцикла сел с трех лет.'
            ],
            [
                'name' => 'Украинки прошли пешком 20 километров ради путевки в Токио',
                'desc' => 'Четыре украинки получили путевки на Олимпиаду в Токио, выполнив норматив на чемпионате Украины по спортивной ходьбе в Луцке на дистанции 20 километров, сообщает «Сегодня». Анна Шевчук стала победитель…'
            ],
            [
                'name' => 'В Днепре прошел чемпионат области по смешанным единоборствам',
                'desc' => 'В Днепре состоялся чемпионат области по смешанным единоборствам ММА. На нем были представлены 160 участников из 20 спортивных клубов Днепра, Кривого Рога, Каменского, Марганца, Покрова и Першотравенск…'
            ],
            [
                'name' => 'Юный спортсмен из Днепра установил новый рекорд Украины',
                'desc' => 'Юный пловец из Днепра, мастер спорта Украины Александр Желтяков установил новый юношеский рекорд Украины по плаванию на дистанцию 100 м на спине с результатом 55,70.'
            ]
        ];
    }

    private function setDataEconomy(): array
    {
        return [
            [
                'name' => 'Дефицит сжиженного газа в Украине на 30% взвинтит цены на АЗС',
                'desc' => 'В течение апреля-мая в Украине ожидается резкий рост стоимости сжиженного газа – ценники на заправках может увеличиться на 5 грн за литр. Об этом UBR.ua рассказал совладелец сети АЗС KLO Игорь Чернявс…'
            ],
            [
                'name' => 'Курс валют на 16 апреля 2021 года',
                'desc' => 'Национальный банк Украины (НБУ) на 16 апреля 2021 года установил официальный курс на уровне 27,95 гривен за доллар. Об этом свидетельствуют данные на сайте регулятора. Официальный курс гривны к доллар…'
            ],
            [
                'name' => 'В Украине начались перебои с поставкой топлива',
                'desc' => 'Некоторые заправки отказываются продавать бензин в розницу. В отдельных сетях АЗС в Украине начались перебои с продажей бензина в розницу, несмотря на то, что в целом цены на топливо снижаются. Опроше…'
            ]
        ];
    }

    private function setDataAuto(): array
    {
        return [
            [
                'name' => 'Всех покупателей б/у авто обяжут пройти обязательный техосмотр',
                'desc' => 'В 2022 году в Украину должен вернуться обязательный техосмотр для некоммерческого легкового транспорта. 5 апреля Министерство инфраструктуры сделало еще один шаг к цели, опубликовав третью редакцию со…'
            ],
            [
                'name' => 'Украинцы смогут официально купить электрокар Nissan Leaf',
                'desc' => 'Электромобиль японского производства Nissan Leaf станет официально доступным в Украине уже летом этого года. О запланированном старте продаж сообщила компания «Ниссан Мотор Украина», пишет UBR.ua. Авт…'
            ],
            [
                'name' => 'Водители нашли новую лазейку, чтобы избежать штрафа за превышение скорости',
                'desc' => 'Суды массово отменяют штрафы Нацполиции за превышение скорости, если правоохранители предоставляют записи с TruCam, не заверенные цифровой подписью. Об этом сообщает UBR.ua, со ссылкой на «Дорожный ад…'
            ]
        ];
    }

}
