<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createNews = new Permission();
        $createNews->name = 'Create news';
        $createNews->slug = 'add-news';
        $createNews->save();

        $editNews = new Permission();
        $editNews->name = 'Edit news';
        $editNews->slug = 'edit-news';
        $editNews->save();

        $deleteNews = new Permission();
        $deleteNews->name = 'Delete news';
        $deleteNews->slug = 'delete-news';
        $deleteNews->save();

        $DeleteCategory = new Permission();
        $DeleteCategory->name = 'Delete category';
        $DeleteCategory->slug = 'delete-category';
        $DeleteCategory->save();

        $CreateCategory = new Permission();
        $CreateCategory->name = 'Create category';
        $CreateCategory->slug = 'create-category';
        $CreateCategory->save();

        $EditCategory = new Permission();
        $EditCategory->name = 'Edit category';
        $EditCategory->slug = 'edit-category';
        $EditCategory->save();
    }
}
