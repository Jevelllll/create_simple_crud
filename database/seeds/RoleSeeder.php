<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newsMaker = new Role();
        $newsMaker->name = 'News Maker';
        $newsMaker->slug = 'news-maker';
        $newsMaker->save();

        $superAdmin= new Role();
        $superAdmin->name = 'Super Admin';
        $superAdmin->slug = 'super-admin';
        $superAdmin->save();
    }
}
