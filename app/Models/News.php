<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class News
 * @package App\Models
 * @property $name
 * @property $desc
 */
class News extends Model
{

    protected $fillable = ['name', 'desc'];

    /**
     * @return mixed
     */
    public function category()
    {
        return $this->belongsTo(CategoryNews::class, 'id', 'news_id');
    }

    /**
     * @return mixed
     */
    public function users()
    {
        return $this->belongsTo(UserNews::class, 'id', 'news_id');
    }

    public static function addNews($data)
    {
        return DB::transaction(
            function () use ($data) {
                try {
                    $category = Category::findOrFail($data['category_id']);
                    $news = new News($data);
                    $news->save();
                    $category->news()->attach($news);
                    auth()->user()->news()->attach($news);
                    return $news;
                } catch (\Exception $e) {
                    return false;
                }
            }
        );
    }
}
