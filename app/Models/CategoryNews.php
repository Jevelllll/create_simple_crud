<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App\Models
 */
class CategoryNews extends Model
{
    protected $table = 'categories_news';
}
