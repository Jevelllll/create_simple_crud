<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App\Models
 * @property $name
 */
class Category extends Model
{

    protected $fillable = ['name'];

    /**
     * @return mixed
     */
    public function news()
    {
        return $this->belongsToMany(News::class, 'categories_news');
    }
}
