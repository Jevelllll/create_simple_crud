<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserNews
 * @package App\UserNews
 */
class UserNews extends Model
{
    protected $table = 'users_news';
}
