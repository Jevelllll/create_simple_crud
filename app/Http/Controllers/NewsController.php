<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Validator;

class NewsController extends BaseController
{

    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['show', 'index']]);
        $this->middleware('role:null,edit-news')->only('update');
        $this->middleware('role:null,delete-news')->only('destroy');
        $this->middleware('role:null,add-news')->only('store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $news = News::with('category')->get();
        if ($news[0] instanceof News) {
            return $this->sendResponse($news->toArray(), 'News list');
        }
        return $this->sendError('News not found.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $requestData = $request->all();
        $validator = Validator::make(
            $requestData,
            [
                'name' => 'required|min:6',
                'desc' => 'required|min:6',
                'category_id' => 'required'
            ]
        );
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        if (auth()->user() instanceof User) {
            if ($news = News::addNews($requestData) instanceof News) {
                return $this->sendResponse($news, 'News created.', 201);
            } else {
                return $this->sendError('News not found.');
            }
        } else {
            return $this->sendError('Unauthorized. The user needs to be authenticated.', [], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param News $news
     * @return JsonResponse
     */
    public function show(News $news): JsonResponse
    {
        $news = $news->with('category')->get()->first();
        if ($news instanceof News) {
            return $this->sendResponse($news->toArray(), 'News retrieved.');
        }
        return $this->sendError('News not found.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param News $news
     * @return JsonResponse
     */
    public function update(Request $request, News $news): JsonResponse
    {
        $requestData = $request->all();
        $validator = Validator::make(
            $requestData,
            [
                'name' => 'nullable|min:6',
                'desc' => 'nullable|min:6'
            ]
        );

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if ($news->users->user_id === auth()->id()) {
            if ($news->update($requestData)) {
                return $this->sendResponse($news->toArray(), 'News update.');
            }
        } else {
            return $this->sendError(
                ' Forbidden. The user is authenticated, but does not have the permissions to perform an action.',
                [],
                403
            );
        }

        return $this->sendError('News not update.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param News $news
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(News $news): JsonResponse
    {
        if (auth()->user()->hasRole('super-admin') || $news->users->user_id === auth()->id()) {
            $news->delete();
            return response()->json(null, 204);
        } else {
            return $this->sendError(
                ' Forbidden. The user is authenticated, but does not have the permissions to perform an action.',
                [],
                403
            );
        }
    }
}
