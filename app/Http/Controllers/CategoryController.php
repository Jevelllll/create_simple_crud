<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CategoryController extends BaseController
{

    public function __construct()
    {
        $this->middleware('jwt.verify', ['except' => ['show', 'index']]);
        $this->middleware('role:super-admin')->only(['update', 'destroy', 'store']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $category = Category::all();
        if ($category[0] instanceof Category) {
            return $this->sendResponse($category, 'Category list');
        } else {
            return $this->sendError('Category not found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $requestData = $request->all();
        $validator = Validator::make(
            $requestData,
            [
                'name' => 'required|min:6',
            ]
        );
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        if ($category = Category::firstOrCreate(['name' => $requestData['name']])) {
            return $this->sendResponse($category, 'Category created.', 201);
        } else {
            return $this->sendError('Category error.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return JsonResponse
     */
    public function show(Category $category): JsonResponse
    {
        $category = $category->with('category')->get()->first();
        if ($category instanceof Category) {
            return $this->sendResponse($category->toArray(), 'Category retrieved.');
        }
        return $this->sendError('Category not found.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Category $category
     * @return JsonResponse
     */
    public function update(Request $request, Category $category): JsonResponse
    {
        $requestData = $request->all();
        $validator = Validator::make(
            $requestData,
            [
                'name' => 'nullable|min:6',
            ]
        );
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        if ($category->update($requestData)) {
            return $this->sendResponse($category->toArray(), 'Category update.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return JsonResponse
     */
    public function destroy(Category $category): JsonResponse
    {

        try {
            $category->news()->delete();
            $category->delete();
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }
        return response()->json(null, 204);
    }
}
